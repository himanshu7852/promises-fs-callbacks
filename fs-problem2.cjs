/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require("fs");
const path = require("path");

function readWriteModifyFiles(txtFile)
{
    problem1(txtFile)
    .then((data)=>{
        return problem2(data)
    })

    .then((newFile2)=>{
        return problem3(newFile2)
    })

   .then((newFile3)=>{
        return problem4(newFile3)
    })

    .then((newFile4)=>{
        problem5(newFile4)
    })

    .catch((error)=>{
        console.log(error)
    })
}
//1. Read the given file lipsum.txt
function problem1(txtFile) 
{
    return new Promise((resolve , reject)=>
    {
        fs.readFile(`${txtFile}`, "UTF-8", (error, data) =>
        {
            if(error)
            {
                reject(error);
            }
            else{
                console.log(data);
                resolve(data);
            }
        })
    })
}

//2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
function problem2(txtFileData)
{
    return new Promise((resolve , reject)=>
    {
        fs.writeFile("filenames.txt", "", (error) => 
        {
            if (error)
            {
                reject(error);
            }
        });
        let newFile = "newFile1.txt";
        fs.writeFile(`${newFile}`, `${txtFileData}`.toUpperCase(), (error) =>
        {
            if (error) 
            {
                reject(error);
            }
            fs.appendFile("filenames.txt", `${newFile} `, (error) =>
            {
                if (error) 
                {
                    reject(error)
                }
                else{
                    resolve(newFile)
                }
            })

        })

    })
}  

//3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
function problem3(txtFile3)
{
    return new Promise((resolve, reject)=>
    {
        fs.readFile(`${txtFile3}`, "UTF-8", (error, data) =>
        {
            if (error)
            {
                reject(error)
            } 
            else{
                console.log('\n')
                console.log(data)
                let converted = data.toLowerCase().split(". ").join("\n")
                const newFile2 = "newFile2.txt"
                fs.writeFile(`${newFile2}`, `${converted}`, (error) =>
                {
                    if (error)
                    {
                        reject(error)
                    }
                    fs.appendFile("filenames.txt", `${newFile2} `, (error) =>
                    {
                        if (error)
                        {
                            reject(error)
                        }
                        else{
                            resolve(newFile2);
                        }
                    });

                       

                });
            }
         }); 
    })       
}

//4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
function problem4(txtFile4)
{
     return new Promise((resolve, reject)=>{

        fs.readFile(`${txtFile4}`, "UTF-8", (error, data) => {
            if (error) 
            {
                reject(error);
            } else 
            {
                console.log('\n');
                console.log(data);
                let sortedData = data.split('.').sort().join('');
                //  console.log(sortedcontent)

                let newFile4 = "newFile3.txt";
                fs.writeFile(`${newFile4}`, `${sortedData}`, (error) => {
                  if (error) 
                   {
                       reject(error);
                   }
                   
                   fs.appendFile("filenames.txt", `${newFile4}`, (error) => {
                      if (error) 
                      {
                          reject(error);
                      }else
                      {
                        resolve('filenames.txt');
                      }
                   });
                }); 
            }   
        });
     });  
}

//5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
    function problem5(txtFile5)
    {
        return new Promise((resolve, reject)=>{

        fs.readFile("filenames.txt", "UTF-8", (error, data) => {
            if (error) 
            {
                reject(error);
            } 
            else{
                console.log('\n');
                console.log(data);

                let dataInFile = data.split(" ");
                for(let index = 0; index < dataInFile.length; index++)
                {
                    if (dataInFile[index] !== "")
                    {
                        fs.unlink(`${dataInFile[index]}`, (error) => {
                            if (error) 
                            {
                            reject(error);
                            } 
                        })
                    }    
                }

                fs.unlink("filenames.txt", (error) => {
                    if (error) 
                    {
                        reject(error);
                    }
                    else
                    {
                        console.log('\n');
                        console.log('Deleted SuccessFully')
                        resolve('All File Deleted')
                    }
                });
            }
        })
        })   
    }

module.exports = readWriteModifyFiles;


