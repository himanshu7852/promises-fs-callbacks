/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/
const fs=require('fs');
const path=require('path');

function createAndDeleteJSONfile(folderName)
{
    createFolder(folderName)
    .then((numberOfFiles)=>{
        console.log('Folder Created')
        return gererateRandomFile(folderName,numberOfFiles)
    })
    .then((arrayOfFileNames)=>{
        console.log('All Files Created')
        return deleteFiles(arrayOfFileNames,folderName)
    })
    .then((value)=>{
        console.log(value)
        deleteFolder(folderName)
    })
    .catch((error)=>{
        console.log(error)
    })
}

    const createFolder=(folderName)=>{
        return new Promise((resolve,reject)=>{
            fs.mkdir(folderName,(error)=>{
                if(error)
                {
                    reject(error)
                }
                else{
                    const numberOfFiles= Math.floor(Math.random()*10 +2);
                    //console.log(numberOfFiles);        
                    resolve(numberOfFiles)
                }
            })
        })
    }
    function gererateRandomFile(folderName,numberOfFiles)
    {
        return new Promise((resolve,reject)=>{
            let numberOfSuccessExicution=0;
            const arrayOfFileNames=[];
            for(let index=0;index<numberOfFiles;index++)
            {
                const nameOfFile=`${index+1}.json`
                fs.writeFile(`${folderName}/${nameOfFile}`,'',(error,value)=>{
                    if(error)
                    {
                        reject(error)
                    }
                    else{
                        numberOfSuccessExicution+=1
                        //console.log(`${nameOfFile} created`)
                        arrayOfFileNames.push(nameOfFile);                
                    }
                    if(numberOfSuccessExicution===numberOfFiles)
                    {
                        resolve(arrayOfFileNames);
                    }
                })
            }

        })
    }

    function deleteFiles(arrayOfFileNames,folder)
    {
        return new Promise((resolve,reject)=>{
            let count=0;
            for(let index=0;index<arrayOfFileNames.length;index++)
            {
                fs.unlink(`${folder}/${arrayOfFileNames[index]}`,(error)=>{
                    count++
    
                    if(count===arrayOfFileNames.length)
                    {
                        if(error)
                        {
                            reject(error)
                        }
                    }
                    else{
                        resolve(`file deleted`)
                    }
                })
            }
        })
    }
    function deleteFolder(folder)
    {
        return new Promise((resolve,reject)=>{
            fs.rmdir(`${folder}`,(error)=>{
                if(error)
                {
                    reject(error)
                }
                else{
                    resolve('Folder deleted')
                }
            })
        })
    }

module.exports=createAndDeleteJSONfile;
